//
//  ViewController.swift
//  MHGOAuthDemo
//
//  Created by Heguang Miao on 19/11/2015.
//  Copyright © 2015 Heguang Miao. All rights reserved.
//

import UIKit
import MHGOAuth_Swift

class ViewController: UIViewController {
    
    var oauthEngine:OAuthEngine = {
        var googleConfig = GoogleOAuthConfig(clientId: "534370830160-44e4c37vg89lb5unu7ono9vbp9bg34co.apps.googleusercontent.com",
            clientSecret: "Tb8aNVCVoju8Wkl4exKMrFg7")
        googleConfig.scopes = ["https://www.googleapis.com/auth/calendar.readonly"]
        return OAuthEngine(config:googleConfig)
    }()
    
    var oauthEngineMS:OAuthEngine = {
        var outlookConfig = OutlookOAuthConfig(clientId: "1676bd4a-bc89-4109-bd7e-25beb35b60e4",
            clientSecret: "AoEQn4dc0Kfi2vyPXpDDymK")
        outlookConfig.scopes = ["https://outlook.office.com/Calendars.Read"]
        return OAuthEngine(config:outlookConfig)
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func beginGoogleAuth(sender:UIButton) {
        let webVC = self.oauthEngine.startAuth { (error, tokenBundle) -> Void in
            if let tokenBundle = tokenBundle {
                let gCalendarReader = GoogleCalendarReader(tokenBundle: tokenBundle)
                gCalendarReader.readCalendar({ (error, response) -> Void in
                    if let response = response {
                        let str = NSString(data: response, encoding: NSUTF8StringEncoding)
                        print(str)
                    } else {
                        print(error)
                    }
                })
            }
        }
        let webVCNav = UINavigationController(rootViewController: webVC)
        self.presentViewController(webVCNav, animated: true, completion: nil)
    }
    
    
    @IBAction func beginOutlookAuth(sender:UIButton) {
        let webVC = self.oauthEngineMS.startAuth { (error, tokenBundle) -> Void in
            if let tokenBundle = tokenBundle {
                let oCalendarReader = OutlookCalendarReader(tokenBundle: tokenBundle)
                oCalendarReader.readCalendar({ (error, response) -> Void in
                    if let response = response {
                        let str = NSString(data: response, encoding: NSUTF8StringEncoding)
                        print(str)
                    } else {
                        print(error)
                    }
                })
            }
        }
        let webVCNav = UINavigationController(rootViewController: webVC)
        self.presentViewController(webVCNav, animated: true, completion: nil)
    }

    
}

