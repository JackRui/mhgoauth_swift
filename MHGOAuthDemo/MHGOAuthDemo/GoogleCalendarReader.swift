//
//  GoogleCalendarReader.swift
//  MHGOAuthDemo
//
//  Created by Heguang Miao on 26/11/2015.
//  Copyright © 2015 Heguang Miao. All rights reserved.
//

import Foundation
import MHGOAuth_Swift

class GoogleCalendarReader : CalendarReadable {
    var tokenBundle:TokenBundle
    
    init(tokenBundle:TokenBundle){
        self.tokenBundle = tokenBundle
    }
    
    func readCalendar(callback: (error:NSError?, response:NSData?) -> Void) {
        let request = NSMutableURLRequest(URL: NSURL(string: "https://www.googleapis.com/calendar/v3/users/me/calendarList")!)
        request.addValue(tokenBundle.accessToken, forHTTPHeaderField: "Authorization")
        let sessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        sessionConfiguration.requestCachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        
        let session = NSURLSession(configuration: sessionConfiguration)
        let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) in
            if let error = error {
                callback(error: error, response: nil)
            } else {
                callback(error: nil,
                    response: data)
            }
        })
        task.resume()
    }
}