//
//  OAuthBaseEngine.swift
//  MHGOAuth-Swift
//
//  Created by Heguang Miao on 19/11/2015.
//  Copyright © 2015 Heguang Miao. All rights reserved.
//

import UIKit

public class OAuthEngine {
    
    var config:OAuthConfig
    var callback:((error:NSError?, tokenBundle:TokenBundle?)->Void)?
    var requestTask:NSURLSessionTask? = nil
    
    public init(config:OAuthConfig) {
        self.config = config
    }
    
    // The auth code is in the response.
    // If the response is valid, request for the access token.
    public func authorizeResponse(resp:String?) {
        if let resp = resp {
            let request = self.config.accessTokenRequestWithAuthorizeResponse(resp)
            if let request = request {
                let sessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
                sessionConfiguration.requestCachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
                
                let session = NSURLSession(configuration: sessionConfiguration)
                self.requestTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) in
                    if self.callback != nil {
                        if let error = error {
                            self.callback!(error: error, tokenBundle: nil)
                        } else {
                            self.callback!(error: nil,
                                tokenBundle: self.config.extractTokenBundleFromAccessTokenResponse(data!))
                        }
                    }
                })
                self.requestTask?.resume()
                return
            }
        }
        let errorInfo = ["error":"Wrong authorization response"]
        let e = NSError(domain: "info.herkuang.OAuth", code: -1, userInfo: errorInfo)
        if self.callback != nil {
            self.callback!(error: e, tokenBundle: nil)
        }
    }
    
    /**
     Entry point
     
     - parameter callback:    Callback when the oauth finishes
     
     - returns: An OAuthWebViewController instance. 
        The caller should be responsible for displaying this view controller either by pushing it
        into the navigation stack or presenting as modal view controller.
     */
    public func startAuth(callback:(error:NSError?, tokenBundle:TokenBundle?)->Void) -> UIViewController {
        self.callback = callback
        let webVC = OAuthWebViewController()
        webVC.callbackURI = self.config.callbackURI
        webVC.URL = self.config.authorizeURL
        webVC.authorizeResponseBlock = { self.authorizeResponse($0)}
        return webVC
    }
    
    /**
     Cancel the OAuth
     */
    public func cancel() {
        self.requestTask?.cancel()
    }
}