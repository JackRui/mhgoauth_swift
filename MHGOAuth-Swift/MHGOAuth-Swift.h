//
//  MHGOAuth-Swift.h
//  MHGOAuth-Swift
//
//  Created by Heguang Miao on 19/11/2015.
//  Copyright © 2015 Heguang Miao. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MHGOAuth-Swift.
FOUNDATION_EXPORT double MHGOAuth_SwiftVersionNumber;

//! Project version string for MHGOAuth-Swift.
FOUNDATION_EXPORT const unsigned char MHGOAuth_SwiftVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MHGOAuth_Swift/PublicHeader.h>


