//
//  TokenBundle.swift
//  MHGOAuth_Swift
//
//  Created by Heguang Miao on 19/11/2015.
//  Copyright © 2015 Heguang Miao. All rights reserved.
//

import Foundation

/**
 *  Token bundle includes all necessary tokens and associated information about tokens
 */
public struct TokenBundle : CustomStringConvertible {
    public var createAt = NSDate()
    public var accessToken:String = ""
    public var accessTokenExpireIn = 0
    public var refreshToken:String?
    public var idToken:String?
    public var idTokenExpireIn = 0
    
    /// This is for debugging
    public var description:String {
        get{
            return "createAt:\(createAt)\naccessToken:\(accessToken)\n"
        }
    }
    
    /// Check if the access token is expired
    public var accessTokenExpired:Bool {
        get {
            let now = NSDate()
            let timePassed = now.timeIntervalSinceDate(createAt)
            return timePassed - Double(accessTokenExpireIn) > 0
        }
    }
    
    // TODO: serialization
}