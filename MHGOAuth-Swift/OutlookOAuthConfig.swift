//
//  OutlookOAuthConfig.swift
//  MHGOAuth_Swift
//
//  Created by RuiJiankun on 3/12/2015.
//  Copyright © 2015 Heguang Miao. All rights reserved.
//

import Foundation

public struct OutlookOAuthConfig : OAuthConfig {
    
    public init(clientId:String, clientSecret:String) {
        self.clientId = clientId
        self.clientSecret = clientSecret
    }
    
    //cId = 1676bd4a-bc89-4109-bd7e-25beb35b60e4
    //cSec = AoEQn4dc0Kfi2vyPXpDDymK
    //scope = scope=wl.signin%20wl.calendars%20wl.basic
    
    public var clientId = ""
    public var clientSecret:String?
    
    public var scopes:[String]? = [""]
    
    public var authorizeURL:NSURL {
        get {
            var urlParams = [String:String]()
            
            if let scopes = scopes {
                var scopesStr = ""
                for aScope in scopes {
                    scopesStr += aScope.stringByEscapingForURLArgument()!
                    if aScope != scopes.last {
                        scopesStr += "+" // This plus symbol should not be URL encoded.
                    }
                }
                urlParams["scope"] = scopesStr
            }
            urlParams["redirect_uri"] = self.callbackURI.stringByEscapingForURLArgument()
            urlParams["response_type"] = "token"
            urlParams["client_id"] = self.clientId
            urlParams["approval_prompt"] = "force"
            urlParams["access_type"] = "offline"
            
            //let baseURL = "https://login.live.com/oauth20_authorize.srf?"
            let baseURL = "https://login.microsoftonline.com/common/oauth2/v2.0/authorize?"
            
            // Note that the "withEncoding" param should be false because of the "+" symbol.
            let fullURL = baseURL + urlParams.toURLParamString(false)!
            return NSURL(string: fullURL)!
        }
    }
    
    public var callbackURI:String = "https://mhgoauth.herkuang.info/callback"
    
    public func accessTokenRequestWithAuthorizeResponse(response:String) -> NSURLRequest? {
        let respDict = Dictionary<String,String>.fromURLParams(response)
        if let code = respDict?["code"] {
            let url = NSURL(string: "https://login.microsoftonline.com/common/oauth2/v2.0/token")!
            let request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = "POST"
            let bodyDict = ["code":code,"redirect_uri":self.callbackURI,"client_id":self.clientId,"client_secret":self.clientSecret!,"scope":"","grant_type":"authorization_code"]
            let urlParams = bodyDict.toURLParamString(true)
            print(urlParams)
            if let urlParams = urlParams {
                let data = urlParams.dataUsingEncoding(NSUTF8StringEncoding)
                request.HTTPBody = data
                return request
            }
        }
        
        return nil
        
    }
    public func extractTokenBundleFromAccessTokenResponse(response:NSData) -> TokenBundle? {
        do {
            let dict = (try NSJSONSerialization.JSONObjectWithData(response, options: NSJSONReadingOptions.AllowFragments)) as! Dictionary<String,AnyObject>
            if let refreshToken = dict["refresh_token"] as? String,
                accessToken = dict["access_token"] as? String,
                expiresIn = dict["expires_in"] as? Int,
                tokenType = dict["token_type"] as? String
            {
                var tokenBundle = TokenBundle()
                // Note that for google apis, the token type plus the access token will be added in the "Authorization" header in later requests,
                // so here I regard the compound string as the "access token"
                tokenBundle.accessToken = tokenType + " " + accessToken
                tokenBundle.refreshToken = refreshToken
                tokenBundle.accessTokenExpireIn = expiresIn
                return tokenBundle
            }
        } catch _ {}
        
        return nil
    }
}

